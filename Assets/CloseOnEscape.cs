﻿using System;
using UniPlay.Core.Engine.Startup;
using UniPlay.Core.Ticking;
using UniPlay.Core.Time;
using UnityEngine;

namespace DefaultNamespace
{
    public static class CloseOnEscape
    {
        [StartupSystem(10)]
        private static void Startup()
        {
            EngineTickers.UpdateTicker.OnTick += OnTick;
        }

        private static void OnTick(ITime time)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Debug.Log("Exiting...");
                Application.Quit();
            }
        }
    }
}