﻿using Game.World;
using UnityEngine;

namespace DefaultNamespace
{
    public class ForceTimeScale : MonoBehaviour
    {
        private void Start()
        {
            TimeScaleModels.Default.TimeScale = 1f;
        }
    }
}