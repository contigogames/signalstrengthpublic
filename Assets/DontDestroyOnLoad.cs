﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class DontDestroyOnLoad : MonoBehaviour
{
    // Use this for initialization
    private void Start()
    {
        DontDestroyOnLoad(gameObject);
    }
}