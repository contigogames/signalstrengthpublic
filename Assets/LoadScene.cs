﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
    [SerializeField]
    private string m_scene;
    
    void Start()
    {
        SceneManager.LoadScene(m_scene);
    }
}