﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Title
{
    public class LoadLevelOnEnter : StateMachineBehaviour
    {
        [SerializeField]
        private string m_levelName;

        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            base.OnStateEnter(animator, stateInfo, layerIndex);

            SceneManager.LoadScene(m_levelName);
        }
    }
}