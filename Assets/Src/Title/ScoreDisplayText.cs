﻿using Game.Systems;
using UnityEngine;

namespace Title
{
    public class ScoreDisplayText : MonoBehaviour
    {
        [SerializeField]
        private TextMesh m_textMesh;

        [SerializeField]
        private ScoreSystemModel m_scoreSystemModel;


        private void Start()
        {
            m_textMesh.text = "x" + m_scoreSystemModel.Score;
        }
    }
}