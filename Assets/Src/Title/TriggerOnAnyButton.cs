﻿using UnityEngine;

namespace Title
{
    public class TriggerOnAnyButton : StateMachineBehaviour
    {
        [SerializeField]
        private string m_trigger;

        public override void OnStateUpdate(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
        {
            if (Input.anyKeyDown)
            {
                animator.SetTrigger(m_trigger);
            }
        }
    }
}