﻿using Game.Visuals;
using UniPlay.Core.View;

namespace Game.Effects
{
    public class PlayEffectOnDestroyed
    {
        public PlayEffectOnDestroyed(IGameObjectView gameObjectView, IEffectView effectView)
        {
            gameObjectView.OnDestroyed += view => effectView.Play();
        }
    }
}