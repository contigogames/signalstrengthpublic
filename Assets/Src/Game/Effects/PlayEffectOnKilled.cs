﻿using Game.Damage;
using Game.Visuals;

namespace Game.Effects
{
    public class PlayEffectOnKilled
    {
        public PlayEffectOnKilled(IDeathModel deathModel, IEffectView effectView)
        {
            deathModel.OnKilled += model => effectView.Play();
        }
    }
}