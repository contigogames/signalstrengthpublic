﻿using System;
using Game.Movement;
using Game.Visuals;
using UniPlay.Core.GameObject;
using UniPlay.Core.Ticking;
using UniPlay.Core.Time;
using UnityEngine;

namespace Game.Player
{
    public sealed class FollowSignalHolderLogic
    {
        private readonly INavigationView m_navigationView;
        private readonly ICameraOwnerModel m_cameraOwnerModel;

        private ICameraOwnerModel m_signalHolderCameraModel;
        private GameObject m_signalHolder;

        public FollowSignalHolderLogic(
            ITicker ticker,
            ISignalHolderModel signalHolderModel,
            INavigationView navigationView,
            ICameraOwnerModel cameraOwnerModel)
        {
            m_navigationView = navigationView;
            m_cameraOwnerModel = cameraOwnerModel;

            signalHolderModel.OnSignalHolderChanged += OnSignalHolderChanged;
            OnSignalHolderChanged(signalHolderModel, null);
            
            ticker.OnTick += OnTick;
        }

        private void OnSignalHolderChanged(ISignalHolderModel signalHolderModel, GameObject gameObject)
        {
            m_signalHolder = signalHolderModel.SignalHolder;
            if (signalHolderModel.SignalHolder)
            {
                m_signalHolderCameraModel =
                    signalHolderModel.SignalHolder.GetActor().GetModel<ICameraOwnerModel>();
            }
        }

        private void OnTick(ITime time)
        {
            if (m_signalHolder)
            {
                var targetPosition = ConvertToVfxSpace(
                    m_signalHolder.transform.position,
                    m_signalHolderCameraModel.CameraOwner.SceneView);

                m_navigationView.MoveTowards(targetPosition, 1f);
            }
        }

        private Vector3 ConvertToVfxSpace(Vector3 position, Camera originalCamera)
        {
            return m_cameraOwnerModel.CameraOwner.SceneView.ScreenToWorldPoint(
                originalCamera.WorldToScreenPoint(position));
        }
    }
}