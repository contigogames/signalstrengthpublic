﻿using System;
using UniPlay.Core.GameObject;
using UniPlay.Core.Logic;
using UniPlay.Core.View;
using UnityEngine;

namespace Game.Player
{
    public class EnableWithSignal
    {
        private readonly ILogic m_target;
        private readonly IGameObjectView m_gameObjectView;

        public EnableWithSignal(ILogic target, IGameObjectView gameObjectView, ISignalHolderModel signalHolderModel)
        {
            m_target = target;
            m_gameObjectView = gameObjectView;
            signalHolderModel.OnSignalHolderChanged += OnSignalChanged;
        }

        private void OnSignalChanged(ISignalHolderModel signalHolderModel, GameObject gameObject)
        {
            m_target.Enabled = signalHolderModel.SignalHolder.GetActor() == m_gameObjectView.Root.GetActor();
        }
    }
}