﻿using UniPlay.Core.View;

namespace Game.Player
{
    public sealed class MaxSignalOnHoldLogic
    {
        public MaxSignalOnHoldLogic(
            ISignalHolderModel signalHolderModel,
            ISignalStrengthModel signalStrengthModel,
            IGameObjectView gameObjectView,
            int maxStrength = 4)
        {
            signalHolderModel.OnSignalHolderChanged += (model, o) =>
            {
                if (model.SignalHolder == gameObjectView.Root)
                {
                    signalStrengthModel.Strength = maxStrength;
                }
            };
        }
    }
}