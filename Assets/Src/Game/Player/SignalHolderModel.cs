﻿using System;
using UniPlay.Core.Ticking;
using UniPlay.Core.Time;
using UnityEngine;

namespace Game.Player
{
    [CreateAssetMenu]
    public class SignalHolderModel
        : ScriptableObject,
            ISignalHolderModel
    {
        public event Action<ISignalHolderModel, GameObject> OnSignalHolderChanged;
        public event Action<ISignalHolderModel> OnSignalCooldownFinished;

        [SerializeField]
        private float m_signalCooldown = 1f;

        private GameObject m_signalHolder;

        private float m_cooldownTimer;


        public GameObject SignalHolder
        {
            get { return m_signalHolder; }
            set
            {
                var old = m_signalHolder;
                if (old != value)
                {
                    if (m_cooldownTimer >= m_signalCooldown)
                    {
                        m_cooldownTimer = 0;
                        EngineTickers.UpdateTicker.OnTick += OnTick;
                        
                        m_signalHolder = value;
                        OnSignalHolderChanged?.Invoke(this, old);
                    }
                }
            }
        }

        private void OnEnable()
        {
            m_signalHolder = null;
            m_cooldownTimer = m_signalCooldown;
        }

        private void OnTick(ITime time)
        {
            m_cooldownTimer += time.DeltaTime;
            if (m_cooldownTimer >= m_signalCooldown)
            {
                EngineTickers.UpdateTicker.OnTick -= OnTick;
                OnSignalCooldownFinished?.Invoke(this);
            }
        }
    }
}