﻿using System;
using System.Collections.Generic;
using Game.Movement;
using Game.Visuals;
using UniPlay.Core.Actor;
using UniPlay.Core.View;

namespace Game.Player
{
    internal sealed class BoltSignalIndicatorActor : Actor
    {
        public static BoltSignalIndicatorActor MakeActor(
            ITickView tickView,
            ITransformView transitionTransformView,
            INavigationView transitionNavigatonView,
            IEffectView transitionEffectView,
            ISignalHolderModel signalHolderModel,
            ICameraOwnerModel cameraOwnerModel)
        {
            var useFollowLogic = true;

            return new BoltSignalIndicatorActor(
                MakerFunc(
                    tickView,
                    transitionTransformView,
                    transitionNavigatonView,
                    transitionEffectView,
                    signalHolderModel,
                    cameraOwnerModel,
                    useFollowLogic));
        }

        private static IEnumerable<object> MakerFunc(
            ITickView tickView,
            ITransformView transitionTransformView,
            INavigationView transitionNavigatonView,
            IEffectView transitionEffectView,
            ISignalHolderModel signalHolderModel,
            ICameraOwnerModel cameraOwnerModel,
            bool useFollowLogic)
        {
            if (useFollowLogic)
            {
                var followSignalHolder = new FollowSignalHolderLogic(
                    tickView.Ticker,
                    signalHolderModel,
                    transitionNavigatonView,
                    cameraOwnerModel);
            }
            else
            {
                var moveToSignalHolder = new MoveToSignalHolderLogic(
                    transitionNavigatonView,
                    transitionTransformView,
                    transitionEffectView,
                    signalHolderModel,
                    cameraOwnerModel.CameraOwner.SceneView);
            }

            yield return tickView;
        }

        private BoltSignalIndicatorActor(IEnumerable<object> maker) : base(maker) { }
    }
}