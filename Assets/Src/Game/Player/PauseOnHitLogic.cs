﻿using System;
using Game.World;
using UniPlay.Core.Ticking;
using UniPlay.Core.Time;

namespace Game.Player
{
    public sealed class PauseOnHitLogic
    {
        private readonly ITicker m_ticker;
        private readonly IPauseModel m_pauseModel;
        private readonly IFloatSettingModel m_freezeTimeModel;

        private float m_timer = 0f;

        public PauseOnHitLogic(
            ITicker ticker,
            IPauseModel pauseModel,
            ISignalStrengthModel signalStrengthModel,
            IFloatSettingModel freezeTimeModel)
        {
            m_ticker = ticker;
            m_pauseModel = pauseModel;
            m_freezeTimeModel = freezeTimeModel;

            signalStrengthModel.OnSignalStrengthChanged += OnSignalStrenthChanged;
        }

        private void OnSignalStrenthChanged(ISignalStrengthModel signalStrengthModel, int i)
        {
            if (signalStrengthModel.Strength < i && signalStrengthModel.Strength > 0)
            {
                m_timer = 0f;
                m_pauseModel.SetPaused(HitPauseType.Default, true);
                m_ticker.OnTick -= OnTick;
                m_ticker.OnTick += OnTick;
            }
        }

        private void OnTick(ITime time)
        {
            m_timer += time.DeltaTime;
            if (m_timer > m_freezeTimeModel.Value)
            {
                m_pauseModel.SetPaused(HitPauseType.Default, false);
                m_ticker.OnTick -= OnTick;
            }
        }
    }
}