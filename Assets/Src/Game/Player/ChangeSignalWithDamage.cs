﻿using Game.Damage;
using UnityEngine;

namespace Game.Player
{
    public class ChangeSignalWithDamage
    {
        public ChangeSignalWithDamage(IDamageSignal damageSignal, ISignalStrengthModel signalStrengthModel)
        {
            damageSignal.OnDamaged += (f, o) => signalStrengthModel.Strength -= Mathf.RoundToInt(f);
        }
    }
}