﻿using System;
using System.Collections.Generic;
using Game.Damage;
using Game.Input;
using Game.Movement;
using Game.Visuals;
using UniPlay.Core.Actor;
using UniPlay.Core.Time;
using UniPlay.Core.View;
using UnityEngine;

namespace Game.Player
{
    [RequireComponent(typeof(GameObjectView))]
    [RequireComponent(typeof(LineRenderer))]
    [RequireComponent(typeof(SignalStrengthModel))]
    [RequireComponent(typeof(CameraOwnerModel))]
    public class PlayerActorBinding : ActorCreator
    {
        private INavigationView m_navigationView;
        private IOffsetInputModel m_offsetInputModel;
        private IGameObjectView m_gameObjectView;

        [SerializeField]
        private BaseTickView m_scaledTicker;

        [SerializeField]
        private BaseTickView m_unscaledTicker;

        [SerializeField]
        private Transform m_movementPointer;

        [SerializeField]
        private Transform m_body;

        [SerializeField]
        private MouseSensitivityModel m_mouseSensitivityModel;

        [SerializeField]
        private SignalHolderModel m_signalHolderModel;

        [SerializeField]
        private string m_signalButton;

        [SerializeField]
        private bool m_startWithSignal;

        [SerializeField]
        private CollisionView m_collisionView;

        [Serializable]
        private struct SignalTakenEffects
        {
            public ParticleSystem SignalTakenParticleSystem;

            public AudioSource SignalTakenAudioSource;

            public ICollection<IEffectView> CreateEffectViews()
            {
                return new IEffectView[]
                {
                    new AudioEffectView(SignalTakenAudioSource),
                    new ParticlesEffectView(SignalTakenParticleSystem, true)
                };
            }
        }

        [SerializeField]
        private SignalTakenEffects m_signalTakenEffects;

        [Serializable]
        private struct HitEffects
        {
            public AudioSource HitAudioSource;

            public ICollection<IEffectView> CreateEffectViews()
            {
                return new IEffectView[]
                {
                    new AudioEffectView(HitAudioSource),
                };
            }
        }

        [SerializeField]
        private HitEffects m_hitEffects;

        [SerializeField]
        private HitPauseTimeModel m_hitPauseTimeModel;

        [Serializable]
        private struct SignalCooldownEffects
        {
            public Renderer SignalCooldownDotRenderer;

            public ICollection<IEffectView> CreateEffectViews()
            {
                return new IEffectView[]
                {
                    new RendererEffectView(SignalCooldownDotRenderer),
                };
            }
        }

        [SerializeField]
        private SignalCooldownEffects m_signalCooldownEffects;

        protected override IActor CreateActor()
        {
            var pointerTransformView = new ClampToCameraTransformView(
                new TransformView(m_movementPointer),
                GetComponent<ICameraOwnerModel>().CameraOwner);

            m_navigationView = new MoveDirectNavigationView(pointerTransformView);
            m_offsetInputModel = new MouseOffsetInputModel(m_mouseSensitivityModel);
            
            return new Actor(ActorMaker());
        }

        private IEnumerable<object> ActorMaker()
        {
            var tickView = m_scaledTicker;
            yield return tickView;

            var signalStrengthModel = new SignalStrengthModel(EngineTimes.ScaledTime);
            var signalStrengthRingsView = GetComponent<SignalStrengthRingsView>();
            signalStrengthModel.OnSignalStrengthChanged += (model, i) =>
                signalStrengthRingsView.ChangeSignalStrengthDisplayed(model.Strength);

            var signalTakenEffect = new MultiEffectView(m_signalTakenEffects.CreateEffectViews());
            var hitEffects = new MultiEffectView(m_hitEffects.CreateEffectViews());

            var signalCooldownEffects = new MultiEffectView(m_signalCooldownEffects.CreateEffectViews());

            var damageSignal = new DamageSignalImpl();
            yield return damageSignal;
            
            var deathModel = new DeathModel();
            yield return deathModel;

            var logicPieces = PlayerActorLogicCreator.Creator(
                m_navigationView,
                m_offsetInputModel,
                tickView,
                m_unscaledTicker,
                m_signalHolderModel,
                signalStrengthModel,
                new UnityButtonInputModel(m_signalButton, tickView.Ticker),
                GetComponent<IGameObjectView>(),
                damageSignal,
                signalTakenEffect,
                hitEffects,
                signalCooldownEffects,
                m_hitPauseTimeModel,
                InternalStartupSignal,
                deathModel,
                m_startWithSignal);

            foreach (var logicPiece in logicPieces)
            {
                yield return logicPiece;
            }

            yield return GetComponentInChildren<ICameraOwnerModel>();
        }
    }
}