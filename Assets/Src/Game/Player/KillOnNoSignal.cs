﻿using Game.Damage;

namespace Game.Player
{
    public class KillOnNoSignal
    {
        public KillOnNoSignal(ISignalStrengthModel signalStrengthModel, IDeathModel deathModel)
        {
            signalStrengthModel.OnSignalStrengthChanged += (model, i) =>
            {
                if (model.Strength <= 0)
                {
                    deathModel.Kill();
                }
            };
        }
    }
}