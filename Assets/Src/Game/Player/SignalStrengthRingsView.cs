﻿using UniPlay.Core.View;
using UnityEngine;

namespace Game.Player
{
    [DisallowMultipleComponent]
    public sealed class SignalStrengthRingsView
        : MonoBehaviour,
            IView
    {
        [SerializeField]
        private GameObject[] m_rings;

        public void ChangeSignalStrengthDisplayed(int newStrength)
        {
            var i = 0;
            for (; i < newStrength - 1 && i < m_rings.Length; i++)
            {
                m_rings[i].SetActive(true);
            }

            for (; i < m_rings.Length; i++)
            {
                m_rings[i].SetActive(false);
            }
        }
    }
}