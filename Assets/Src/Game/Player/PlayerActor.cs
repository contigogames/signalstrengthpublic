﻿using System.Collections.Generic;
using System.Linq;
using Game.Damage;
using Game.Input;
using Game.Movement;
using Game.Visuals;
using Game.World;
using UniPlay.Core.Actor;
using UniPlay.Core.Model;
using UniPlay.Core.Ticking;
using UniPlay.Core.View;
using UnityEngine;

namespace Game.Player
{
    public static class PlayerActorLogicCreator
    {
        public static IEnumerable<object> Creator(
            INavigationView navigationView,
            IOffsetInputModel inputModel,
            ITickView tickView,
            ITickView unscaledTickView,
            ISignalHolderModel signalHolderModel,
            ISignalStrengthModel signalStrengthModel,
            IButtonInputModel buttonInputModel,
            IGameObjectView gameObjectView,
            IDamageSignal damageSignal,
            IEffectView signalTakenEffect,
            IEffectView hitEffect,
            IEffectView signalCooldownFinishedEffect,
            IFloatSettingModel hitPauseTimeMode,
            IStartupSignal startupSignal,
            IDeathModel deathModel,
            bool startWithSignal)
        {
            // Take the signal when the player button is pressed.
            var takeSignalOnButtonLogic = new TakeSignalOnButtonLogic(
                signalHolderModel,
                buttonInputModel,
                gameObjectView);

            // Move with the offset input.
            var moveWithInput = new MoveWithOffsetInput(navigationView, inputModel, tickView.Ticker);

            // Enable input when getting the signal.
            var enableMovementWithSignal = new EnableWithSignal(moveWithInput, gameObjectView, signalHolderModel);

            if (startWithSignal)
            {
                startupSignal.OnStartup += actor => signalHolderModel.SignalHolder = gameObjectView.Root;
            }

            var maxStrengthWhenTakeSignal = new MaxSignalOnHoldLogic(
                signalHolderModel,
                signalStrengthModel,
                gameObjectView);

            var signalTakenEffectLogic = new PlayEffectOnTakeSignal(
                signalHolderModel,
                gameObjectView,
                signalTakenEffect);

            var damageSignalStrength = new ChangeSignalWithDamage(damageSignal, signalStrengthModel);
            var killOnNoSignal = new KillOnNoSignal(signalStrengthModel, deathModel);
            
            var playEffectOnHit = new PlayEffectOnHitLogic(hitEffect, signalStrengthModel);
            var freezeOnHit = new PauseOnHitLogic(
                unscaledTickView.Ticker,
                GlobalPauseModel.Instance, 
                signalStrengthModel,
                hitPauseTimeMode);

            var playEffectOnCooldownReady = new PlayEffectOnSignalCooldownFinished(
                signalHolderModel,
                signalCooldownFinishedEffect,
                gameObjectView);

            var loadLevelOnDeath = new LoadLevelOnKilled(deathModel, "GameOver");
            
            return Enumerable.Empty<object>();
        }
    }
}