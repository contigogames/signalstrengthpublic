﻿using Game.Input;
using UniPlay.Core.View;

namespace Game.Player
{
    public sealed class TakeSignalOnButtonLogic
    {
        public TakeSignalOnButtonLogic(
            ISignalHolderModel signalHolderModel,
            IButtonInputModel buttonInputModel,
            IGameObjectView gameObjectView)
        {
            buttonInputModel.OnPressed += model => signalHolderModel.SignalHolder = gameObjectView.Root;
        }
    }
}