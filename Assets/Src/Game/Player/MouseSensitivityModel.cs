﻿using UnityEngine;

namespace Game.Player
{
    [CreateAssetMenu]
    public class MouseSensitivityModel
        : ScriptableObject,
            IFloatSettingModel
    {
        [SerializeField]
        private float m_value = 0.2f;

        public float Value
        {
            get { return m_value; }
            set { m_value = value; }
        }
    }
}