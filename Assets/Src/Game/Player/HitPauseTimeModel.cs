﻿using UnityEngine;

namespace Game.Player
{
    [CreateAssetMenu]
    public class HitPauseTimeModel
        : ScriptableObject,
            IFloatSettingModel
    {
        [SerializeField]
        private float m_value;

        public float Value
        {
            get { return m_value; }
        }
    }
}