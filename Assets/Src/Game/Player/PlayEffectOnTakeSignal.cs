﻿using Game.Visuals;
using UniPlay.Core.View;

namespace Game.Player
{
    public sealed class PlayEffectOnTakeSignal
    {
        public PlayEffectOnTakeSignal(
            ISignalHolderModel signalHolderModel,
            IGameObjectView gameObjectView,
            IEffectView effectView)
        {
            signalHolderModel.OnSignalHolderChanged += (model, o) =>
            {
                if (model.SignalHolder == gameObjectView.Root)
                {
                    effectView.Play();
                }
                else
                {
                    effectView.Stop();
                }
            };
        }
    }
}