﻿using UniPlay.Core.Model;

namespace Game.Player
{
    public interface IFloatSettingModel : IModel
    {
        float Value { get; }
    }
}