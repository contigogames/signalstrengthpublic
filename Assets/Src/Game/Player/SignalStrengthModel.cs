﻿using System;
using UniPlay.Core.Time;

namespace Game.Player
{
    public sealed class SignalStrengthModel : ISignalStrengthModel
    {
        private readonly ITime m_time;
        private int m_strength;
        private float m_lastChangedTime;
        
        private readonly float m_invTime;

        public int Strength
        {
            get { return m_strength; }
            set
            {
                if (value > m_strength || (m_time.Time - m_lastChangedTime) >= m_invTime)
                {
                    var old = m_strength;
                    m_strength = value;
                    if (old != m_strength)
                    {
                        m_lastChangedTime = m_time.Time;
                        OnSignalStrengthChanged?.Invoke(this, old);
                    }
                }
            }
        }

        public event Action<ISignalStrengthModel, int> OnSignalStrengthChanged;

        public SignalStrengthModel(ITime time, int startingStrength = 4, float invTime = 0.1f)
        {
            m_time = time;
            m_strength = startingStrength;
            m_invTime = invTime;
        }
    }
}