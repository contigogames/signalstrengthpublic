﻿using System;
using Game.Movement;
using Game.Visuals;
using UniPlay.Core.Actor;
using UniPlay.Core.View;
using UnityEngine;

namespace Game.Player
{
    [RequireComponent(typeof(GenericTickView))]
    [RequireComponent(typeof(CameraOwnerModel))]
    public sealed class BoltSignalIndicatorActorBinding : ActorCreator
    {
        [SerializeField]
        private SignalHolderModel m_signalHolderModel;

        [SerializeField]
        private TrailRenderer m_trailRenderer;

        [SerializeField]
        private float m_speed;

        protected override IActor CreateActor()
        {
            var transformView = new TransformView(transform);
            var tickView = GetComponent<ITickView>();
            var navigationView = new MoveTowardsNavigationView(tickView.Ticker, transformView, m_speed);
            var transitionEffect = new TrailEffectView(m_trailRenderer);
            var cameraOwnerModel = GetComponent<ICameraOwnerModel>();


            return BoltSignalIndicatorActor.MakeActor(
                tickView,
                transformView,
                navigationView,
                transitionEffect,
                m_signalHolderModel,
                cameraOwnerModel);
        }
    }
}