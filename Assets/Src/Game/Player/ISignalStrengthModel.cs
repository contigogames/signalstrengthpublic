﻿using System;

namespace Game.Player
{
    public interface ISignalStrengthModel
    {
        int Strength { get; set; }

        event Action<ISignalStrengthModel, int> OnSignalStrengthChanged;
    }
}