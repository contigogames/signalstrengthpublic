﻿using Game.Visuals;
using UniPlay.Core.View;
using UnityEngine;

namespace Game.Player
{
    public sealed class PlayEffectOnSignalCooldownFinished
    {
        public PlayEffectOnSignalCooldownFinished(
            ISignalHolderModel signalHolderModel,
            IEffectView effectView,
            IGameObjectView owner = null)
        {
            signalHolderModel.OnSignalCooldownFinished += model =>
            {
                if (owner == null || owner.Root == model.SignalHolder)
                {
                    effectView.Play();
                }
                else
                {
                    effectView.Stop();
                }
            };

            if (owner != null)
            {
                signalHolderModel.OnSignalHolderChanged += (model, o) =>
                {
                    if (model.SignalHolder != owner.Root)
                    {
                        effectView.Stop();
                    }
                };
            }
        }
    }
}