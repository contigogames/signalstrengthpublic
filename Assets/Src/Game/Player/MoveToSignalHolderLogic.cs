﻿using System;
using Game.Movement;
using Game.Visuals;
using UniPlay.Core.GameObject;
using UniPlay.Core.View;
using UnityEngine;

namespace Game.Player
{
    public sealed class MoveToSignalHolderLogic
    {
        private readonly INavigationView m_navigationView;
        private readonly ITransformView m_transformView;
        private readonly IEffectView m_trailEffectView;
        private readonly Camera m_ownerCamera;

        public MoveToSignalHolderLogic(
            INavigationView navigationView,
            ITransformView transformView,
            IEffectView trailEffectView,
            ISignalHolderModel signalHolderModel,
            Camera ownerCamera)
        {
            m_navigationView = navigationView;
            m_transformView = transformView;
            m_trailEffectView = trailEffectView;
            m_ownerCamera = ownerCamera;

            signalHolderModel.OnSignalHolderChanged += OnSignalHolderChanged;
        }

        private void OnSignalHolderChanged(ISignalHolderModel signalHolderModel, GameObject gameObject)
        {
            m_trailEffectView.Stop();

            if (gameObject)
            {
                var sourceCameraView = gameObject.GetActor().GetModel<ICameraOwnerModel>().CameraOwner;
                var sourcePosition = ConvertToVfxSpace(gameObject.transform.position, sourceCameraView.SceneView);

                var targetCameraView =
                    signalHolderModel.SignalHolder.GetActor().GetModel<ICameraOwnerModel>().CameraOwner;
                var targetPosition = ConvertToVfxSpace(
                    signalHolderModel.SignalHolder.transform.position,
                    targetCameraView.SceneView);

                m_transformView.Position = sourcePosition;
                m_trailEffectView.Play();
                m_navigationView.MoveTowards(targetPosition, 1f);
            }
        }

        private Vector3 ConvertToVfxSpace(Vector3 position, Camera sourceCamera)
        {
            return m_ownerCamera.ScreenToWorldPoint(sourceCamera.WorldToScreenPoint(position));
        }
    }
}