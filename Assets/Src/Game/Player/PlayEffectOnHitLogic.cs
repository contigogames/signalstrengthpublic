﻿using Game.Visuals;

namespace Game.Player
{
    public sealed class PlayEffectOnHitLogic
    {
        public PlayEffectOnHitLogic(IEffectView effectView, ISignalStrengthModel signalStrengthModel)
        {
            signalStrengthModel.OnSignalStrengthChanged += (model, i) =>
            {
                if (model.Strength < i)
                {
                    effectView.Play();
                }
            };
        }
    }
}