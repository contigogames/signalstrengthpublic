﻿using System;
using UniPlay.Core.View;
using UnityEngine;

namespace Game.Player
{
    public sealed class LoseSignalOnCollisionLogic
    {
        private readonly ISignalStrengthModel m_signalStrengthModel;

        public LoseSignalOnCollisionLogic(ICollisionView collisionView, ISignalStrengthModel signalStrengthModel)
        {
            m_signalStrengthModel = signalStrengthModel;
            collisionView.OnCollisionEntered += OnCollisionEntered;
        }

        private void OnCollisionEntered(GameObject obj)
        {
            m_signalStrengthModel.Strength -= 1;
        }
    }
}