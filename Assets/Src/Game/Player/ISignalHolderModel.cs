﻿using System;
using UniPlay.Core.Model;
using UnityEngine;

namespace Game.Player
{
    public interface ISignalHolderModel : IModel
    {
        GameObject SignalHolder { get; set; }

        event Action<ISignalHolderModel, GameObject> OnSignalHolderChanged;
        event Action<ISignalHolderModel> OnSignalCooldownFinished;
    }
}