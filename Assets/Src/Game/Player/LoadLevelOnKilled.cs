﻿using Game.Damage;
using UnityEngine.SceneManagement;

namespace Game.Player
{
    public sealed class LoadLevelOnKilled
    {
        public LoadLevelOnKilled(IDeathModel deathModel, string targetLevel)
        {
            deathModel.OnKilled += model => SceneManager.LoadScene(targetLevel);
        }
    }
}