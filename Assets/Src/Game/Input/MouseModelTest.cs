﻿using Game.Player;
using UniPlay.Core.Time;
using UniPlay.Core.View;
using UnityEngine;

namespace Game.Input
{
    [RequireComponent(typeof(GenericTickView))]
    public class MouseModelTest : MonoBehaviour
    {
        public Vector2 MouseDelta;
        public MouseSensitivityModel MouseSensitivityModel;

        private readonly MouseOffsetInputModel m_model;

        private void Start()
        {
            GetComponent<ITickView>().Ticker.OnTick += OnTick;
        }

        private void OnTick(ITime time)
        {
            MouseDelta = m_model.MouseDelta;
        }
    }
}