﻿using System;
using UniPlay.Core.Ticking;
using UniPlay.Core.Time;
using UnityEngine;

namespace Game.Input
{
    public sealed class UnityButtonInputModel : IButtonInputModel
    {
        private readonly string m_inputName;
        public event Action<IButtonInputModel> OnPressed;
        public event Action<IButtonInputModel> OnReleased;
        
        public bool IsDown
        {
            get { return UnityEngine.Input.GetButton(m_inputName); }
        }

        public UnityButtonInputModel(string inputName, ITicker ticker)
        {
            m_inputName = inputName;
            
            ticker.OnTick += OnTick;
        }

        private void OnTick(ITime time)
        {
            if (UnityEngine.Input.GetButtonDown(m_inputName))
            {
                OnPressed?.Invoke(this);
            }
            else if (UnityEngine.Input.GetButtonUp(m_inputName))
            {
                OnReleased?.Invoke(this);
            }
        }
    }
}