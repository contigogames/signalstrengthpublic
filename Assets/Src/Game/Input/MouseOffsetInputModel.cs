﻿using Game.Player;
using Game.World;
using UniPlay.Core.Model;
using UnityEngine;

namespace Game.Input
{
    public interface IOffsetInputModel : IModel
    {
        Vector3 MouseDelta { get; }
    }

    public class MouseOffsetInputModel : IOffsetInputModel
    {
        private readonly IFloatSettingModel m_mouseSensitivityModel;

        public Vector3 MouseDelta
        {
            get
            {
                var x = UnityEngine.Input.GetAxis("Mouse X") *
                        m_mouseSensitivityModel.Value *
                        TimeScaleModels.Default.TimeScale;
                var y = UnityEngine.Input.GetAxis("Mouse Y") *
                        m_mouseSensitivityModel.Value *
                        TimeScaleModels.Default.TimeScale;

                return new Vector3(x, y);
            }
        }

        public MouseOffsetInputModel(IFloatSettingModel mouseSensitivityModel)
        {
            m_mouseSensitivityModel = mouseSensitivityModel;
        }
    }
}