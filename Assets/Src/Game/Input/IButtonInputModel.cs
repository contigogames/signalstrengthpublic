﻿using System;

namespace Game.Input
{
    public interface IButtonInputModel
    {
        event Action<IButtonInputModel> OnPressed;
        event Action<IButtonInputModel> OnReleased;
        
        bool IsDown { get; }
    }
}