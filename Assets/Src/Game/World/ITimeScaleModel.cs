﻿using UniPlay.Core.Model;

namespace Game.World
{
    public interface ITimeScaleModel : IModel
    {
        float TimeScale { get; set; }
    }
}