﻿namespace Game.World
{
    public sealed class HitPauseType : IPauseType
    {
        public static readonly HitPauseType Default = new HitPauseType();
        
        private HitPauseType() { }
    }
}