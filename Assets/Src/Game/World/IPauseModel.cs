﻿using System;
using System.Collections.Generic;
using UniPlay.Core.Model;

namespace Game.World
{
    public interface IPauseType { }

    public interface IPauseModel : IModel
    {
        event Action<IPauseModel> OnPaused;
        event Action<IPauseModel> OnUnpaused;

        IReadOnlyCollection<IPauseType> GetAllPausedTypes();
        bool GetPaused(IPauseType type);
        void SetPaused(IPauseType type, bool value);
    }
}