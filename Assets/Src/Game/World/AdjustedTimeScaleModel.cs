﻿using UnityEngine;

namespace Game.World
{
    public class AdjustedTimeScaleModel : ITimeScaleModel
    {
        public static readonly AdjustedTimeScaleModel Instance = new AdjustedTimeScaleModel();

        public float TimeScale
        {
            get { return RawTimeScaleModel.Instance.TimeScale; }
            set { RawTimeScaleModel.Instance.TimeScale = value; }
        }

        private AdjustedTimeScaleModel() { }
    }
}