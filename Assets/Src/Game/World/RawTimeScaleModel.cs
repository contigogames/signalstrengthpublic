﻿using UniPlay.Core.Model;
using UnityEngine;

namespace Game.World
{
    public sealed class RawTimeScaleModel : ITimeScaleModel
    {
        public static readonly RawTimeScaleModel Instance = new RawTimeScaleModel();

        public float TimeScale
        {
            get { return Time.timeScale; }
            set { Time.timeScale = value; }
        }

        private RawTimeScaleModel() { }
    }
}