﻿namespace Game.World
{
    public static class TimeScaleModels
    {
        public static readonly RawTimeScaleModel Raw = RawTimeScaleModel.Instance;
        public static readonly AdjustedTimeScaleModel Adjusted = AdjustedTimeScaleModel.Instance;
        public static readonly ITimeScaleModel Default = Adjusted;
    }
}