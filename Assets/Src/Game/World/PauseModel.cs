﻿using System;
using System.Collections.Generic;

namespace Game.World
{
    public sealed class PauseModel : IPauseModel
    {
        private readonly ITimeScaleModel m_timeScaleModel;
        private readonly HashSet<IPauseType> m_pauseTypes = new HashSet<IPauseType>();

        public event Action<IPauseModel> OnPaused;
        public event Action<IPauseModel> OnUnpaused;

        public PauseModel(ITimeScaleModel timeScaleModel)
        {
            m_timeScaleModel = timeScaleModel;
        }

        public IReadOnlyCollection<IPauseType> GetAllPausedTypes()
        {
            return m_pauseTypes;
        }

        public bool GetPaused(IPauseType type)
        {
            return m_pauseTypes.Contains(type);
        }

        public void SetPaused(IPauseType type, bool value)
        {
            var wasEmpty = m_pauseTypes.Count == 0;

            if (value)
            {
                m_pauseTypes.Add(type);
            }
            else
            {
                m_pauseTypes.Remove(type);
            }

            var isEmpty = m_pauseTypes.Count == 0;

            if (wasEmpty && !isEmpty)
            {
                m_timeScaleModel.TimeScale = 0f;
                OnPaused?.Invoke(this);
            }

            if (!wasEmpty && isEmpty)
            {
                m_timeScaleModel.TimeScale = 1f;
                OnUnpaused?.Invoke(this);
            }
        }
    }
}