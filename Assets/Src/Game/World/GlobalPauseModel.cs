﻿namespace Game.World
{
    public static class GlobalPauseModel
    {
        public static readonly PauseModel Instance = new PauseModel(TimeScaleModels.Default);
    }
}