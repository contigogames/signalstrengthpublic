﻿using UniPlay.Core.View;

namespace Game.Visuals
{
    public interface IEffectView : IView
    {
        void Play();
        void Stop();
    }
}