﻿using System.Collections.Generic;

namespace Game.Visuals
{
    public class MultiEffectView : IEffectView
    {
        private readonly List<IEffectView> m_effectViews;

        public MultiEffectView(ICollection<IEffectView> effectViews)
        {
            m_effectViews = new List<IEffectView>(effectViews);
        }
        
        public void Play()
        {
            foreach (var effectView in m_effectViews)
            {
                effectView.Play();
            }
        }

        public void Stop()
        {
            foreach (var effectView in m_effectViews)
            {
                effectView.Stop();
            }
        }
    }
}