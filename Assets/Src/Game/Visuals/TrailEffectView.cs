﻿using UnityEngine;

namespace Game.Visuals
{
    public sealed class TrailEffectView : IEffectView
    {
        private readonly TrailRenderer m_trailRenderer;

        public TrailEffectView(TrailRenderer trailRenderer)
        {
            m_trailRenderer = trailRenderer;
        }
        
        public void Play()
        {
            m_trailRenderer.enabled = true;
        }

        public void Stop()
        {
            m_trailRenderer.enabled = false;
        }
    }
}