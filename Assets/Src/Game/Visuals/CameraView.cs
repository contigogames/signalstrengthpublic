﻿using UnityEngine;

namespace Game.Visuals
{
    public sealed class CameraView
        : MonoBehaviour,
            ICameraView
    {
        [SerializeField]
        private Camera m_sceneView;

        public Camera SceneView
        {
            get { return m_sceneView; }
        }
    }
}