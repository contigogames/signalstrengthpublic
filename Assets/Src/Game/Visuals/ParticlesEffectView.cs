﻿using UnityEngine;

namespace Game.Visuals
{
    public sealed class ParticlesEffectView : IEffectView
    {
        private readonly ParticleSystem m_particleSystem;
        private readonly bool m_withChildren;

        public ParticlesEffectView(ParticleSystem particleSystem, bool withChildren)
        {
            m_particleSystem = particleSystem;
            m_withChildren = withChildren;
        }
        
        public void Play()
        {
            m_particleSystem.Play(m_withChildren);
        }

        public void Stop()
        {
            m_particleSystem.Stop(true, ParticleSystemStopBehavior.StopEmitting);
        }
    }
}