﻿using UniPlay.Core.Spawning;
using UniPlay.Core.View;
using UnityEngine;

namespace Game.Visuals
{
    public class PrefabSpawnEffectView : IEffectView
    {
        private readonly GameObject m_prefab;
        private readonly TransformView m_prefabSpawnLocation;
        private readonly bool m_applyRotation;
        private readonly ISpawner<GameObject> m_prefabSpawner = GameObjectSpawner.Default;

        public PrefabSpawnEffectView(GameObject prefab, TransformView prefabSpawnLocation, bool applyRotation = false)
        {
            m_prefab = prefab;
            m_prefabSpawnLocation = prefabSpawnLocation;
            m_applyRotation = applyRotation;
        }
        
        public void Play()
        {
            var position = m_prefabSpawnLocation.Position;
            var rotation = m_prefabSpawnLocation.Rotation;
            m_prefabSpawner.Spawn(m_prefab).OnFullfilled += o =>
            {
                o.transform.position = position;

                if (m_applyRotation)
                {
                    o.transform.rotation = rotation;
                }
            };
        }

        public void Stop()
        {
        }
    }
}