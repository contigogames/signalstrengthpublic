﻿using UnityEngine;

namespace Game.Visuals
{
    public sealed class RendererEffectView : IEffectView
    {
        private readonly Renderer m_renderer;

        public RendererEffectView(Renderer renderer)
        {
            m_renderer = renderer;
        }

        public void Play()
        {
            m_renderer.enabled = true;
        }

        public void Stop()
        {
            m_renderer.enabled = false;
        }
    }
}