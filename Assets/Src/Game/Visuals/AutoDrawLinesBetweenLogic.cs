﻿using System;
using System.Collections.Generic;
using UniPlay.Core.Ticking;
using UniPlay.Core.Time;
using UniPlay.Core.View;
using UnityEngine;

namespace Game.Visuals
{
    public class AutoDrawLinesBetweenLogic
    {
        private readonly IList<ITransformView> m_targets;
        private readonly LineRenderer m_lineRenderer;
        private readonly ITicker m_ticker;

        public AutoDrawLinesBetweenLogic(ICollection<ITransformView> targets, LineRenderer lineRenderer, ITicker ticker)
        {
            m_targets = new List<ITransformView>(targets);
            m_lineRenderer = lineRenderer;
            m_ticker = ticker;

            lineRenderer.positionCount = targets.Count;
            
            m_ticker.OnTick += OnTick;
        }

        private void OnTick(ITime time)
        {
            for (var i = 0; i < m_targets.Count && i < m_lineRenderer.positionCount; i++)
            {
                m_lineRenderer.SetPosition(i, m_targets[i].Position);
            }
        }
    }
}