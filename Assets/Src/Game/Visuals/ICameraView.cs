﻿using UniPlay.Core.View;
using UnityEngine;

namespace Game.Visuals
{
    public interface ICameraView : IView
    {
        Camera SceneView { get; }
    }
}