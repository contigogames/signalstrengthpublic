﻿using UniPlay.Core.Model;

namespace Game.Visuals
{
    public interface ICameraOwnerModel : IModel
    {
        ICameraView CameraOwner { get; }
    }
}