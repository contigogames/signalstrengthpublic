﻿using UnityEngine;

namespace Game.Visuals
{
    public sealed class AudioEffectView : IEffectView
    {
        private readonly AudioSource m_audioSource;

        public AudioEffectView(AudioSource audioSource)
        {
            m_audioSource = audioSource;
        }

        public void Play()
        {
            m_audioSource.Play();
        }

        public void Stop()
        {
            m_audioSource.Stop();
        }
    }
}