﻿using UnityEngine;

namespace Game.Visuals
{
    [DisallowMultipleComponent]
    public sealed class CameraOwnerModel
        : MonoBehaviour,
            ICameraOwnerModel
    {
        [SerializeField]
        private CameraView m_cameraOwner;

        public ICameraView CameraOwner
        {
            get { return m_cameraOwner; }
        }
    }
}