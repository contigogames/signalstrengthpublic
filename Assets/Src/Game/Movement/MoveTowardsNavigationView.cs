﻿using System;
using System.Collections;
using UniPlay.Core.Ticking;
using UniPlay.Core.Time;
using UniPlay.Core.View;
using UnityEngine;

namespace Game.Movement
{
    public class MoveTowardsNavigationView : INavigationView
    {
        private readonly ITicker m_ticker;
        private readonly ITransformView m_transformView;
        private readonly float m_speed;
        private readonly float m_threashold;

        public event Action<INavigationView> OnStartMovingToTarget;
        public event Action<INavigationView> OnReachedTarget;

        public Vector3 CurrentTarget { get; private set; }

        private float m_scaler;

        private enum State
        {
            AtTarget,
            MovingToTarget
        }

        private State m_currentMovementState = State.AtTarget;

        public MoveTowardsNavigationView(
            ITicker ticker,
            ITransformView transformView,
            float speed,
            float threashold = 0.05f)
        {
            m_ticker = ticker;
            m_transformView = transformView;
            m_speed = speed;
            m_threashold = threashold;

            CurrentTarget = m_transformView.Position;
            m_ticker.OnTick += OnTick;
        }

        private void OnTick(ITime time)
        {
            var currentPosition = m_transformView.Position;
            var offset = (currentPosition - CurrentTarget).magnitude;

            if (m_currentMovementState == State.AtTarget)
            {
                if (offset > m_threashold)
                {
                    m_currentMovementState = State.MovingToTarget;
                    OnStartMovingToTarget?.Invoke(this);
                }
            }

            if (m_currentMovementState == State.MovingToTarget)
            {
                if (offset <= m_threashold)
                {
                    m_currentMovementState = State.AtTarget;
                    OnReachedTarget?.Invoke(this);
                }
                else
                {
                    var nextPosition = Vector3.MoveTowards(
                        currentPosition,
                        CurrentTarget,
                        time.DeltaTime * m_speed * m_scaler);
                    m_transformView.Position = nextPosition;
                }
            }
        }

        public void MoveTowards(Vector3 position, float scaler)
        {
            CurrentTarget = position;
            m_scaler = scaler;
        }

        public void Stop()
        {
            CurrentTarget = m_transformView.Position;
        }
    }
}