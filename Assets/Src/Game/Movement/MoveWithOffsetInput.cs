﻿using System;
using Game.Input;
using UniPlay.Core.Logic;
using UniPlay.Core.Ticking;
using UniPlay.Core.Time;
using UnityEngine;

namespace Game.Movement
{
    public class MoveWithOffsetInput : ILogic
    {
        private readonly INavigationView m_navigationView;
        private readonly IOffsetInputModel m_offsetInputModel;
        private readonly ITicker m_ticker;
        private bool m_enabled;

        public bool Enabled
        {
            get { return m_enabled; }
            set
            {
                var old = m_enabled;
                m_enabled = value;

                if (old != m_enabled)
                {
                    if (value)
                    {
                        m_ticker.OnTick += OnTick;
                    }
                    else
                    {
                        m_ticker.OnTick -= OnTick;
                    }
                }
            }
        }

        public MoveWithOffsetInput(INavigationView navigationView, IOffsetInputModel offsetInputModel, ITicker ticker)
        {
            m_navigationView = navigationView;
            m_offsetInputModel = offsetInputModel;
            m_ticker = ticker;
        }

        private void OnTick(ITime time)
        {
            var newPosition = m_navigationView.CurrentTarget + m_offsetInputModel.MouseDelta;
            m_navigationView.MoveTowards(newPosition, 1f);
        }
    }
}