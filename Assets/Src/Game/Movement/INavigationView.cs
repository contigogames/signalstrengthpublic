﻿using System;
using UniPlay.Core.View;
using UnityEngine;

namespace Game.Movement
{
    public interface INavigationView : IView
    {
        event Action<INavigationView> OnStartMovingToTarget;
        event Action<INavigationView> OnReachedTarget;
        
        Vector3 CurrentTarget { get; }
        
        void MoveTowards(Vector3 position, float scaler);
        void Stop();
    }
}