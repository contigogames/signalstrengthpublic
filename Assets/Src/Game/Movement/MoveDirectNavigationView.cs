﻿using System;
using UniPlay.Core.View;
using UnityEngine;

namespace Game.Movement
{
    public class MoveDirectNavigationView : INavigationView
    {
        private readonly ITransformView m_trasnformView;
        public event Action<INavigationView> OnStartMovingToTarget;
        public event Action<INavigationView> OnReachedTarget;

        public Vector3 CurrentTarget
        {
            get { return m_trasnformView.Position; }
        }

        public MoveDirectNavigationView(ITransformView trasnformView)
        {
            m_trasnformView = trasnformView;
        }

        public void MoveTowards(Vector3 position, float scaler)
        {
            OnStartMovingToTarget?.Invoke(this);

            var currentPosition = m_trasnformView.Position;

            RaycastHit hit;
            if (Physics.Linecast(currentPosition, position, out hit))
            {
                position = hit.point;
            }

            m_trasnformView.Position = position;

            OnReachedTarget?.Invoke(this);
        }

        public void Stop() { }
    }
}