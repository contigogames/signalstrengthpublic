﻿using Game.Visuals;
using UniPlay.Core.View;
using UnityEngine;

namespace Game.Movement
{
    public class ClampToCameraTransformView : ITransformView
    {
        private readonly ITransformView m_transformViewImplementation;
        private readonly ICameraView m_cameraView;

        public Vector3 Position
        {
            get { return m_transformViewImplementation.Position; }
            set
            {
                var lowerLeft = m_cameraView.SceneView.ViewportToWorldPoint(Vector3.zero);
                var upperLeft = m_cameraView.SceneView.ViewportToWorldPoint(Vector3.up);
                var upperRight = m_cameraView.SceneView.ViewportToWorldPoint(Vector3.one);
                var lowerRight = m_cameraView.SceneView.ViewportToWorldPoint(Vector3.right);

                var newX = Mathf.Clamp(value.x, lowerLeft.x, lowerRight.x);
                var newY = Mathf.Clamp(value.y, lowerLeft.y, upperLeft.y);

                m_transformViewImplementation.Position = new Vector3(newX, newY, value.z);
            }
        }

        public Quaternion Rotation
        {
            get { return m_transformViewImplementation.Rotation; }
            set { m_transformViewImplementation.Rotation = value; }
        }

        public ClampToCameraTransformView(ITransformView transformViewImplementation, ICameraView cameraView)
        {
            m_transformViewImplementation = transformViewImplementation;
            m_cameraView = cameraView;
        }
    }
}