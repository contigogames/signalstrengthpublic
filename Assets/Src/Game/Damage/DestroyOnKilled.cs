﻿using UniPlay.Core.View;

namespace Game.Damage
{
    public class DestroyOnKilled
    {
        public DestroyOnKilled(IGameObjectView gameObjectView, IDeathModel deathModel)
        {
            deathModel.OnKilled += model => gameObjectView.Destroy();
        }
    }
}