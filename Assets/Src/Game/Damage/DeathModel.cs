﻿using System;
using UniPlay.Core.Actor;
using UniPlay.Core.Model;
using UniPlay.Core.Signal;

namespace Game.Damage
{
    public interface IDeathModel : IModel
    {
        bool IsDead { get; }

        event Action<IDeathModel> OnKilled;

        void Kill();
    }

    public class DeathModel : IDeathModel
    {
        public bool IsDead { get; private set; }
        
        public event Action<IDeathModel> OnKilled;

        public void Kill()
        {
            if (!IsDead)
            {
                IsDead = true;
                OnKilled?.Invoke(this);
            }
        }
    }
}