﻿using UnityEngine;

namespace Game.Damage
{
    [CreateAssetMenu]
    public class ConstantDamageCalculator : SendDamageOnCollisionBehaviour.DamageCalculatorAsset
    {
        [SerializeField]
        private float m_damage;
        
        public override float Calculate(GameObject gameObject)
        {
            return m_damage;
        }
    }
}