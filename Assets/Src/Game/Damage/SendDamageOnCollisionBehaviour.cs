﻿using System;
using UniPlay.Core.View;
using UnityEngine;
using UnityEngine.Events;

namespace Game.Damage
{
    [RequireComponent(typeof(CollisionView))]
    public class SendDamageOnCollisionBehaviour : MonoBehaviour
    {
        public abstract class DamageCalculatorAsset : ScriptableObject
        {
            public abstract float Calculate(GameObject gameObject);
        }

        public abstract class DamagePredicateAsset : ScriptableObject
        {
            public abstract bool Get(GameObject gameObject);
        }

        [SerializeField]
        private DamageCalculatorAsset m_damageCalculatorAsset;

        [SerializeField]
        private DamagePredicateAsset m_damagePredicateAsset;

        private void Start()
        {
            Predicate<GameObject> predicate = null;
            if (m_damagePredicateAsset)
            {
                predicate = m_damagePredicateAsset.Get;
            }
            
            var logic = new SendDamageOnCollision(
                GetComponent<CollisionView>(),
                m_damageCalculatorAsset.Calculate,
                predicate);
        }
    }
}