﻿using System;
using UniPlay.Core.Signal;

namespace Game.Damage
{
    public interface IDamageSignalSender : ISignalSender
    {
        void Damage(float amount, object instigator);
    }

    public interface IDamageSignal
    {
        event Action<float, object> OnDamaged;
    }

    public class DamageSignalImpl
        : IDamageSignal,
            IDamageSignalSender
    {
        public event Action<float, object> OnDamaged;

        public void Damage(float amount, object instigator)
        {
            OnDamaged?.Invoke(amount, instigator);
        }
    }
}