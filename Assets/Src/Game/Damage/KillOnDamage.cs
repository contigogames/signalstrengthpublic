﻿namespace Game.Damage
{
    public class KillOnDamage
    {
        public KillOnDamage(IDamageSignal damageSignal, IDeathModel deathModel)
        {
            damageSignal.OnDamaged += (f, o) => deathModel.Kill();
        }
    }
}