﻿using UniPlay.Core.View;

namespace Game.Damage
{
    public class DestroyOnDamageLogic
    {
        public DestroyOnDamageLogic(IDamageSignal damageSignal, IGameObjectView gameObjectView)
        {
            damageSignal.OnDamaged += (f, o) => gameObjectView.Destroy();
        }
    }
}