﻿using System;
using UniPlay.Core.GameObject;
using UniPlay.Core.Logic;
using UniPlay.Core.View;
using UnityEngine;

namespace Game.Damage
{
    public class SendDamageOnCollision : ILogic
    {
        private readonly ICollisionView m_collisionView;
        private readonly Predicate<GameObject> m_collisionPredicate;
        private readonly bool m_startEnabled;
        private readonly Func<GameObject, float> m_damageCalculator;
        private bool m_enabled;

        public bool Enabled
        {
            get { return m_enabled; }
            set
            {
                var old = m_enabled;
                m_enabled = value;
                if (old != m_enabled)
                {
                    if (m_enabled)
                    {
                        m_collisionView.OnCollisionEntered += OnCollisionEnter;
                    }
                    else
                    {
                        m_collisionView.OnCollisionEntered -= OnCollisionEnter;
                    }
                }
            }
        }

        public SendDamageOnCollision(
            ICollisionView collisionView,
            Func<GameObject, float> damageCalculator = null,
            Predicate<GameObject> collisionPredicate = null,
            bool startEnabled = true)
        {
            m_damageCalculator = damageCalculator;
            m_collisionView = collisionView;
            m_collisionPredicate = collisionPredicate ?? (o => true);
            Enabled = startEnabled;
        }

        private void OnCollisionEnter(GameObject gameObject)
        {
            if (m_collisionPredicate(gameObject))
            {
                gameObject.GetActor().GetView<IDamageSignalSender>().Damage(m_damageCalculator(gameObject), gameObject);
            }
        }
    }
}