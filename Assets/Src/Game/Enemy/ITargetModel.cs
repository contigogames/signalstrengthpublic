﻿using UniPlay.Core.Model;
using UniPlay.Core.View;

namespace Game.Enemy
{
    public interface ITargetModel : IModel
    {
        IGameObjectView Target { get; set; }
    }
}