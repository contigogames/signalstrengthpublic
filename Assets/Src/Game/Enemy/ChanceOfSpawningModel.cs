﻿using Game.Player;
using UnityEngine;

namespace Game.Enemy
{
    [CreateAssetMenu]
    public class ChanceOfSpawningModel
        : ScriptableObject,
            IFloatSettingModel
    {
        [SerializeField]
        private AnimationCurve m_chanceOfSpawningCurve = AnimationCurve.Linear(0, 0, 100, 0.9f);

        public float Value
        {
            get { return m_chanceOfSpawningCurve.Evaluate(Time.timeSinceLevelLoad); }
        }
    }
}