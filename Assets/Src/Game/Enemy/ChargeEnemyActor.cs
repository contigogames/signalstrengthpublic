﻿using System;
using System.Collections.Generic;
using Game.Damage;
using Game.Effects;
using Game.Movement;
using Game.Visuals;
using UniPlay.Core.Actor;
using UniPlay.Core.View;

namespace Game.Enemy
{
    public class ChargeEnemyActor : Actor
    {
        private readonly ITargetModel m_targetModel;

        public ChargeEnemyActor(
            ITargetModel targetModel,
            INavigationView navigationView,
            ITransformView transformView,
            ITickView tickView,
            IGameObjectView gameObjectView,
            IDamageSignal damageSignal,
            IDeathModel deathModel,
            IEffectView deathEffectView) : base(
            new object[]
            {
                targetModel,
                navigationView,
                transformView,
                tickView,
                gameObjectView,
                damageSignal,
                deathModel,
                deathEffectView,
                new MoveTowardsTargetLogic(navigationView, targetModel, tickView.Ticker),
                new KillOnDamage(damageSignal, deathModel), 
                new DestroyOnKilled(gameObjectView, deathModel), 
                new PlayEffectOnKilled(deathModel, deathEffectView)
            }) { }
    }
}