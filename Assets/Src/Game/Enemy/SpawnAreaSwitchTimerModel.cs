﻿using Game.Player;
using UnityEngine;

namespace Game.Enemy
{
    [CreateAssetMenu]
    public class SpawnAreaSwitchTimerModel
        : ScriptableObject,
            IFloatSettingModel
    {
        [SerializeField]
        private AnimationCurve m_animationCurve = AnimationCurve.Linear(0, 5, 100, 1);

        public float Value
        {
            get { return m_animationCurve.Evaluate(Time.timeSinceLevelLoad); }
        }
    }
}