﻿using Game.Damage;
using Game.Movement;
using Game.Systems;
using Game.Visuals;
using UniPlay.Core.Actor;
using UniPlay.Core.View;
using UnityEngine;

namespace Game.Enemy
{
    [RequireComponent(typeof(GenericTickView))]
    [RequireComponent(typeof(GameObjectView))]
    public class ChargeEnemyBinding : ActorCreator
    {
        [SerializeField]
        private float m_speed;

        [SerializeField ]
        private ScoreSystemModel m_scoreSystemModel;

        [SerializeField]
        private GameObject m_killedEffect;

        protected override IActor CreateActor()
        {
            var transformView = new TransformView(transform);
            var genericTickView = GetComponent<GenericTickView>();
            var targetModel = new TargetModel();
            var navigationView = new MoveTowardsNavigationView(genericTickView.Ticker, transformView, m_speed);
            var gameObjectView = GetComponent<GameObjectView>();
            var damageSignal = new DamageSignalImpl();
            var deathModel = new DeathModel();

            damageSignal.OnDamaged += (f, o) => m_scoreSystemModel.Score += 1;

            var killedEffectView = new PrefabSpawnEffectView(m_killedEffect, transformView);

            return new ChargeEnemyActor(
                targetModel,
                navigationView,
                transformView,
                genericTickView,
                gameObjectView,
                damageSignal,
                deathModel,
                killedEffectView);
        }
    }
}