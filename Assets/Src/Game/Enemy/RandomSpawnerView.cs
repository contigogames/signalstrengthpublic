﻿using System.Collections.Generic;
using UniPlay.Core.Actor;
using UniPlay.Core.Tasks;
using UnityEngine;

namespace Game.Enemy
{
    public class RandomSpawnerView : ISpawnerView
    {
        private readonly List<ISpawnerView> m_spawnerViews;

        public RandomSpawnerView(ICollection<ISpawnerView> spawnerViews)
        {
            m_spawnerViews = new List<ISpawnerView>(spawnerViews);
        }
        
        public Promise<IActor> Spawn(Object prefab)
        {
            var spawner = m_spawnerViews[Random.Range(0, m_spawnerViews.Count - 1)];
            return spawner.Spawn(prefab);
        }
    }
}