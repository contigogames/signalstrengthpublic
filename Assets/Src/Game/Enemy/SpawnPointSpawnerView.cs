﻿using UniPlay.Core.Actor;
using UniPlay.Core.Spawning;
using UniPlay.Core.Tasks;
using UniPlay.Core.View;
using UnityEngine;

namespace Game.Enemy
{
    public sealed class SpawnPointSpawnerView
        : MonoBehaviour,
            ISpawnerView
    {
        private readonly ISpawner<IActor> m_actorSpawner = ActorSpawner.Default;

        public Promise<IActor> Spawn(Object prefab)
        {
            var promise = m_actorSpawner.Spawn(prefab);
            promise.OnFullfilled += actor => { actor.GetView<ITransformView>().Position = transform.position; };

            return promise;
        }
    }
}