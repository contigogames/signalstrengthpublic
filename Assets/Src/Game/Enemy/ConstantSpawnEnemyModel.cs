﻿using UnityEngine;

namespace Game.Enemy
{
    [CreateAssetMenu]
    public class ConstantSpawnEnemyModel
        : ScriptableObject,
            ISpawnEnemyModel
    {
        [SerializeField]
        private GameObject m_prefab;

        public GameObject Prefab
        {
            get { return m_prefab; }
        }
    }
}