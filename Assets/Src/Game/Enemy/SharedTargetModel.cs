﻿using UniPlay.Core.View;
using UnityEngine;

namespace Game.Enemy
{
    [CreateAssetMenu]
    public sealed class SharedTargetModel
        : ScriptableObject,
            ITargetModel
    {
        public IGameObjectView Target { get; set; }
    }
}