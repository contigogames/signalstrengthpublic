﻿using System;
using System.Collections.Generic;
using Game.Player;
using UniPlay.Core.Random;
using UniPlay.Core.Ticking;
using UniPlay.Core.Time;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Game.Enemy
{
    public sealed class SpawningLogic
    {
        private readonly IFloatSettingModel m_timerModel;
        private readonly IFloatSettingModel m_chanceOfSpawningModel;
        private readonly List<ISpawnerView> m_spawners;
        private readonly List<ISpawnEnemyModel> m_enemyModels;
        private readonly List<ITargetModel> m_enemyTargets;

        private float m_timeSinceSwitch = 0f;

        private int m_currentSpawner;

        public SpawningLogic(
            IFloatSettingModel chanceOfSpawningModel,
            ICollection<ISpawnerView> spawners,
            ICollection<ISpawnEnemyModel> enemyModels,
            ICollection<ITargetModel> enemyTargets,
            ITicker ticker)
        {
            m_chanceOfSpawningModel = chanceOfSpawningModel;
            m_enemyModels = new List<ISpawnEnemyModel>(enemyModels);
            m_spawners = new List<ISpawnerView>(spawners);
            m_enemyTargets = new List<ITargetModel>(enemyTargets);

            ticker.OnTick += OnTick;
        }

        private void OnTick(ITime time)
        {
            var spawnRoll = UniformRandomDistribution.GetRandomInRange(0f, 1f);

            if (spawnRoll < m_chanceOfSpawningModel.Value * time.DeltaTime)
            {
                var currentSpawner = m_currentSpawner;
                m_spawners[currentSpawner].Spawn(m_enemyModels[currentSpawner].Prefab).OnFullfilled += actor =>
                {
                    actor.GetModel<ITargetModel>().Target = m_enemyTargets[currentSpawner].Target;
                };
                m_currentSpawner = (currentSpawner + 1) % m_spawners.Count;
            }
        }
    }
}