﻿using Game.Movement;
using UniPlay.Core.Ticking;
using UniPlay.Core.Time;
using UnityEngine;

namespace Game.Enemy
{
    public sealed class MoveTowardsTargetLogic
    {
        private readonly INavigationView m_navigationView;
        private readonly ITargetModel m_targetModel;

        public MoveTowardsTargetLogic(INavigationView navigationView, ITargetModel targetModel, ITicker ticker)
        {
            m_navigationView = navigationView;
            m_targetModel = targetModel;
            ticker.OnTick += OnTick;
        }

        private void OnTick(ITime time)
        {
            if (m_targetModel.Target != null)
            {
                m_navigationView.MoveTowards(m_targetModel.Target.Root.transform.position, 1f);
            }
            else
            {
                m_navigationView.Stop();
            }
        }
    }
}