﻿using UniPlay.Core.View;

namespace Game.Enemy
{
    public sealed class DestroyOnCollideLogic
    {
        public DestroyOnCollideLogic(ICollisionView collisionView, IGameObjectView gameObjectView)
        {
            collisionView.OnCollisionEntered += o => gameObjectView.Destroy();
        }
    }
}