﻿using UnityEngine;

namespace Game.Enemy
{
    public interface ISpawnEnemyModel
    {
        GameObject Prefab { get; }
    }
}