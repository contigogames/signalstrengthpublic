﻿using System.Collections.Generic;
using UniPlay.Core.Actor;
using UniPlay.Core.View;
using UnityEngine;

namespace Game.Enemy
{
    [RequireComponent(typeof(GameObjectView))]
    [RequireComponent(typeof(GenericTickView))]
    public class EnemySpawnManagerBinding : ActorCreator
    {
        [SerializeField]
        private ConstantSpawnEnemyModel m_whiteEnemyModel;

        [SerializeField]
        private ConstantSpawnEnemyModel m_blackEnemyModel;

        [SerializeField]
        private GameObject m_whiteSpawnPointsRoot;

        [SerializeField]
        private GameObject m_blackSpawnPointsRoot;

        [SerializeField]
        private ChanceOfSpawningModel m_chanceOfSpawningModel;

        [SerializeField]
        private SpawnAreaSwitchTimerModel m_areaSwitchTimerModel;

        [SerializeField]
        private SharedTargetModel m_whiteTargetModel;

        [SerializeField]
        private SharedTargetModel m_blackTargetModel;

        protected override IActor CreateActor()
        {
            return new Actor(ActorMaker());
        }

        private IEnumerable<object> ActorMaker()
        {
            var tickView = GetComponent<ITickView>();
            yield return tickView;

            var whiteSpawnPoints = m_whiteSpawnPointsRoot.GetComponentsInChildren<ISpawnerView>();
            var whiteSpawner = new RandomSpawnerView(whiteSpawnPoints);

            var blackSpawnPoints = m_blackSpawnPointsRoot.GetComponentsInChildren<ISpawnerView>();
            var blackSpawner = new RandomSpawnerView(blackSpawnPoints);

            var spawners = new[]
            {
                whiteSpawner,
                blackSpawner
            };

            var spawnModels = new[]
            {
                m_whiteEnemyModel,
                m_blackEnemyModel
            };

            var targetModels = new[]
            {
                m_whiteTargetModel,
                m_blackTargetModel
            };

            var spawnLogic = new SpawningLogic(
                m_chanceOfSpawningModel,
                spawners,
                spawnModels,
                targetModels,
                tickView.Ticker);
        }
    }
}