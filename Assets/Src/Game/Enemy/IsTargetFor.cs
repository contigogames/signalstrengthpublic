﻿using UniPlay.Core.View;
using UnityEngine;

namespace Game.Enemy
{
    public sealed class IsTargetFor : MonoBehaviour
    {
        [SerializeField]
        private SharedTargetModel m_targetModel;

        [SerializeField]
        private GameObjectView m_gameObjectView;

        private void OnValidate()
        {
            if (m_gameObjectView == null)
            {
                m_gameObjectView = GetComponent<GameObjectView>();
            }
        }

        private void Awake()
        {
            m_targetModel.Target = m_gameObjectView;
        }
    }
}