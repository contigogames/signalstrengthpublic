﻿using UniPlay.Core.Actor;
using UniPlay.Core.Tasks;
using UniPlay.Core.View;
using UnityEngine;

namespace Game.Enemy
{
    public interface ISpawnerView : IView
    {
        Promise<IActor> Spawn(Object prefab);
    }
}