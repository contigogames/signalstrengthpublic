﻿using UniPlay.Core.View;

namespace Game.Enemy
{
    public class TargetModel : ITargetModel
    {
        public IGameObjectView Target { get; set; }
    }
}