﻿using UnityEngine;

namespace Game.Enemy
{
    public class SpawnEnemyModel
        : MonoBehaviour,
            ISpawnEnemyModel
    {
        [SerializeField]
        private GameObject m_prefab;

        public GameObject Prefab
        {
            get { return m_prefab; }
        }
    }
}