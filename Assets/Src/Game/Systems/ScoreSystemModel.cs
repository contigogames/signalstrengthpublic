﻿using UniPlay.Core.Model;
using UnityEngine;

namespace Game.Systems
{
    [CreateAssetMenu]
    public class ScoreSystemModel
        : ScriptableObject,
            IModel
    {
        public int Score { get; set; }
    }
}