﻿using UnityEngine;

namespace Game.Systems
{
    public class ZeroScoreOnStart : MonoBehaviour
    {
        [SerializeField]
        private ScoreSystemModel m_scoreSystemModel;
        
        private void Start()
        {
            m_scoreSystemModel.Score = 0;
        }
    }
}