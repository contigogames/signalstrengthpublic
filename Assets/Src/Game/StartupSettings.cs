﻿using UniPlay.Core.Engine.Startup;
using UnityEngine;

namespace Game
{
    static class StartupSettings
    {
        [StartupSystem]
        private static void Startup()
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
    }
}