﻿using System;
using System.Diagnostics;
using UniPlay.Core.Engine.Startup;
using UniPlay.Core.Ticking;
using UniPlay.Core.Time;
using UnityEngine.SceneManagement;
using Debug = UnityEngine.Debug;

namespace Core
{
    public static class GcOptomizer
    {
        [StartupSystem]
        private static void OnStartup()
        {
            SceneManager.sceneLoaded += (arg0, mode) =>
            {
                GC.Collect(0);
                GC.Collect(1);
                GC.Collect(2);
            };
        }
    }
}